from dataclasses import dataclass
import numpy as np

from aocd import get_data
data = get_data(day=11)

@dataclass
class Monkey:
    items: list
    op: str
    divMod: int
    nextTarget: tuple
    numItemsInspected: int = 0

    def handle_items(self, findRelief=True, prodMod=None):
        while self.items:
            # inspect item
            self.numItemsInspected += 1
            item = self.items.pop(0)
            expr = self.op.replace('old', str(item))
            newVal = eval(expr)         # increased worry
            if findRelief:
                newVal = int(newVal//3)     # relief
            else:
                newVal %= prodMod
            # decide toss
            idx = int( not (newVal%self.divMod) )
            yield self.nextTarget[idx], newVal

def process(inputData):
    monkeys = []
    for monkey in inputData.split('\n\n'):
        monkeyInfo = monkey.split('\n')
        items = [int(i.strip()) for i in monkeyInfo[1].split(':')[-1].split(',')]
        op = monkeyInfo[2].split(':')[-1].split('=')[-1].strip()
        divMod = int(monkeyInfo[3].split(' ')[-1])
        target1 = int(monkeyInfo[4][-1])
        target0 = int(monkeyInfo[5][-1])
        monkeys.append(Monkey(items,op,divMod,(target0,target1)))
    return monkeys


def run(monkeys, nRounds=20, findRelief=True, prodMod=None):
    for round in range(nRounds):
        for i,monkey in enumerate(monkeys):
            for target, value in monkey.handle_items(findRelief=findRelief, prodMod=prodMod):
                monkeys[target].items.append(value)

# Example
tmp = """\
Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1"""

exampleInputs = process(tmp)
run(exampleInputs)
itemsInspected = sorted([monkey.numItemsInspected for monkey in exampleInputs])
business = itemsInspected[-2] * itemsInspected[-1]
print(business)

exampleInputs = process(tmp)
prodMod = int(np.product([monkey.divMod for monkey in exampleInputs]))
run(exampleInputs, 10000, False, prodMod)
itemsInspected = sorted([monkey.numItemsInspected for monkey in exampleInputs])
business = itemsInspected[-2] * itemsInspected[-1]
print(business)

# Part 1
inputs = process(data)
run(inputs)
itemsInspected = sorted([monkey.numItemsInspected for monkey in inputs])
business = itemsInspected[-2] * itemsInspected[-1]
print(business)

# Part 2
inputs = process(data)
prodMod = int(np.product([monkey.divMod for monkey in inputs]))
run(inputs, 10000, findRelief=False, prodMod=prodMod)
itemsInspected = sorted([monkey.numItemsInspected for monkey in inputs])
business = itemsInspected[-2] * itemsInspected[-1]
print(business)
