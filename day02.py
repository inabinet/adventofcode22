from aocd import get_data
data = get_data(day=2)

def process(inputData):
    return [round.split(' ') for round in inputData.split('\n')]

inputs = process(data)


elfRPS = {
    'A': 1,   #'Rock',
    'B': 2,   #'Paper',
    'C': 3,   #'Scissors',
}

meRPS = {
    'X': 1, # Rock
    'Y': 2, # Paper
    'Z': 3, # Scissors
}

meNorm = {
    'X': -1,  # lose
    'Y': 0,  # draw
    'Z': 1,  # win
}


def calculate_round_score(elf, me):
    myscore = meRPS[me]
    raw = myscore-elfRPS[elf]
    norm = ((raw+1) % 3) - 1
    score = 3 + 3 * norm + myscore
    return score

def calculate_round_score2(elf, me):
    norm = meNorm[me]
    myscore = ((elfRPS[elf]+norm-1) % 3) + 1
    score = 3 + 3 * norm + myscore
    return score


# Example
tmp = """\
A Y
B X
C Z"""

exampleInputs = process(tmp)
scores = [calculate_round_score(*round) for round in exampleInputs]
print(sum(scores))
scores = [calculate_round_score2(*round) for round in exampleInputs]
print(sum(scores))

# Part 1
scores = [calculate_round_score(*round) for round in inputs]
print(sum(scores))

# Part 2
scores = [calculate_round_score2(*round) for round in inputs]
print(sum(scores))

