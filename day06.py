from aocd import get_data
data = get_data(day=6)
inputs = data

def find_start_of_packet_marker(datastream):
    return _find_marker(datastream, 4)

def find_start_of_message_marker(datastream):
    return _find_marker(datastream, 14)

def _find_marker(datastream, nChars):
    for i in range(nChars,len(datastream)):
        if len(set(datastream[i-nChars:i])) == nChars:
            return i

# Example
tmp = """\
mjqjpqmgbljsphdztnvjfqwrcgsmlb
bvwbjplbgvbhsrlpgdmjqwftvncz
nppdvjthqldpwncqszvftbrmjlhg
nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg
zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"""

print([find_start_of_packet_marker(example) for example in tmp.split('\n')])
print([find_start_of_message_marker(example) for example in tmp.split('\n')])

# Part 1
print(find_start_of_packet_marker(inputs))

# Part 2
print(find_start_of_message_marker(inputs))
