import numpy as np
import heapq

from aocd import get_data
data = get_data(day=12)

def process(inputData):
    aVal = ord('a') # 97, but will calculate programmatically anyway
    map = []
    for row in inputData.split('\n'):
        map.append([(ord(char)-aVal) for char in row])
    map = np.array(map)

    # find start
    tmpLoc = np.where(map==-14)
    map[tmpLoc] = 0     # set height to lowest
    start = (tmpLoc[0][0], tmpLoc[1][0])

    # find stop
    tmpLoc = np.where(map == -28)
    map[tmpLoc] = 25    # set height to highest
    stop = (tmpLoc[0][0], tmpLoc[1][0])

    return map, start, stop

inputs = process(data)


def get_neighbors(node, nRows, nCols):
    neighbors = []
    if node[0] != 0:
        neighbors.append((node[0]-1, node[1]))
    if node[1] != 0:
        neighbors.append((node[0], node[1]-1))
    if node[0] != nRows-1:
        neighbors.append((node[0]+1, node[1]))
    if node[1] != nCols-1:
        neighbors.append((node[0], node[1]+1))
    return neighbors

def create_graph(map, part1=True):
    graph = {}
    nRows, nCols = map.shape
    for row in range(nRows):
        for col in range(nCols):
            node = (row,col)
            graph[node]= {}
            for neighbor in get_neighbors(node, nRows, nCols):
                valid = (map[neighbor]-map[node] <= 1) if part1 else (map[neighbor]-map[node] >= -1)
                if valid:
                    graph[node].update({neighbor: 1})
    return graph

# dijkstra algorithm I found last year for day09 puzzle
def calculate_distances(graph, starting_vertex):
    distances = {vertex: float('infinity') for vertex in graph}
    distances[starting_vertex] = 0

    pq = [(0, starting_vertex)]
    while len(pq) > 0:
        current_distance, current_vertex = heapq.heappop(pq)

        # Nodes can get added to the priority queue multiple times. We only
        # process a vertex the first time we remove it from the priority queue.
        if current_distance > distances[current_vertex]:
            continue

        for neighbor, weight in graph[current_vertex].items():
            distance = current_distance + weight

            # Only consider this new path if it's better than any path we've
            # already found.
            if distance < distances[neighbor]:
                distances[neighbor] = distance
                heapq.heappush(pq, (distance, neighbor))

    return distances


# Example
tmp = """\
Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi"""

exampleInputs = process(tmp)
map, start, stop = exampleInputs
graph = create_graph(map)
distances = calculate_distances(graph, start)
print(distances[stop])

graph2 = create_graph(map, part1=False)
distances2 = calculate_distances(graph2, stop)
options = [distances2[startPoint] for startPoint in zip(*np.where(map == 0))]
print(min(options))

# Part 1
map, start, stop = inputs
graph = create_graph(map)
distances = calculate_distances(graph, start)
print(distances[stop])

# Part 2
graph2 = create_graph(map, part1=False)
distances2 = calculate_distances(graph2, stop)
options = [distances2[startPoint] for startPoint in zip(*np.where(map == 0))]
print(min(options))
