from aocd import get_data
data = get_data(day=9)

def process(inputData):
    dirDict = {'R':1, 'L':-1, 'U':1j, 'D':-1j}
    output = []
    for row in inputData.split('\n'):
        dir,steps = row.split(' ')
        output.append(dirDict[dir] * int(steps))
    return output

inputs = process(data)

def run(moves, number_of_knots):
    knots = [0+0j]*number_of_knots   # index 0 will be the head and last index will be the tail
    visited = {knots[-1]}
    for steps in moves:
        stepSize = steps/abs(steps)
        while steps:
            knots[0] += stepSize  # only step head (the others will only follow)
            for idx in range(number_of_knots-1):
                delta = knots[idx] - knots[idx+1]
                if abs(delta) == 2:                  # step in direction (follow)
                    tmpStep = delta/2
                    knots[idx+1] += tmpStep
                elif abs(delta) > 1.5:               # step diagonally
                    tmpStep = complex(delta.real/abs(delta.real), delta.imag/abs(delta.imag))
                    knots[idx+1] += tmpStep

            visited.add(knots[-1])
            steps -= stepSize

    return visited, knots

# Example
tmp = """\
R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2"""

exampleInputs = process(tmp)
visited, *_ = run(exampleInputs, 2)
print(len(visited))

visited, *_ = run(exampleInputs, 10)
print(len(visited))

tmp = """\
R 5
U 8
L 8
D 3
R 17
D 10
L 25
U 20"""

exampleInputs = process(tmp)
visited, *_ = run(exampleInputs, 10)
print(len(visited))

# Part 1
visited, *_ = run(inputs, 2)
print(len(visited))

# Part 2
visited, *_ = run(inputs, 10)
print(len(visited))
