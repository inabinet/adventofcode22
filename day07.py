from dataclasses import dataclass

from aocd import get_data
data = get_data(day=7)

@dataclass
class Directory:
    name: str
    parent: object
    files: dict

    def __getitem__(self, key):
        if key == '..':
            return self.parent

        key = key.strip('/')
        if key == '':  # traverse back to root
            if self.parent is None:
                return self
            else:
                return self.parent[key]

        top, *rest = key.split('/')
        item = self.files[top]
        if isinstance(item, Directory) and len(rest)>0:
            subdir = '/'.join(rest)
            return item[subdir]

        return item

    def __get_files_and_subdirs(self):
        files = []
        subdirs = []
        for tmp,val in self.files.items():
            if isinstance(val, Directory):
                subdirs.append(tmp)
            else:
                files.append(tmp)
        files.sort()
        subdirs.sort()
        return files, subdirs

    def __repr__(self, spacing=''):
        output = f"{spacing}{self.name} ({self.size})\n"
        files, subdirs = self.__get_files_and_subdirs()
        for f in files:
            output += f"{spacing}  {f} ({self.files[f]})\n"
        for d in subdirs:
            #output += f"{spacing}{d} ({self.files[d].get_size()})\n"
            output += self.files[d].__repr__(f"{spacing}  ")
        return output

    @property
    def size(self):
        size = 0
        files, subdirs = self.__get_files_and_subdirs()
        for f in files:
            size += int(self.files[f])
        for d in subdirs:
            size += self.files[d].size
        return size

    def get_directory_sizes(self):
        sizes = [(self.name, self.size)]
        _, subdirs = self.__get_files_and_subdirs()
        for d in subdirs:
            sizes.extend(self.files[d].get_directory_sizes())
        return sizes


def process(inputData):
    root = Directory('/', None, {})
    curdir = '/'

    cursor = 0
    inputList = inputData.split('\n')
    while cursor < len(inputList):
        cmd = inputList[cursor]
        if cmd.startswith('$ '):                    # command
            cmd = cmd[2:]
            if cmd.startswith('cd '):               # change directory command
                path = cmd[3:]
                if path == '/':                     # go back to root
                    curdir = '/'
                elif path == '..':
                    curdir = '/'.join(curdir.rstrip('/').split('/')[:-1])
                    #if curdir == '':
                    curdir += r'/'
                else:
                    subdirs = path.split(r'/')       # go down one (or more) levels
                    for sub in subdirs:
                        if sub not in root[curdir].files:
                            root[curdir].files.update({sub: Directory(sub, root[curdir], {})})
                        curdir += f'{sub}/'
                cursor += 1
            elif cmd.startswith('ls'):
                cursor += 1
                fileDict = root[curdir].files
                while not (filename:=inputList[cursor]).startswith('$'):
                    #filename = inputList[cursor]
                    if filename.startswith('dir'):
                        dirname = filename[4:].strip()
                        fileDict.update({dirname: Directory(dirname,root[curdir],{})})
                    else:
                        size,name = filename.split(' ')
                        fileDict.update({name:size})
                    cursor += 1
                    if cursor >= len(inputList):
                        break
    return root


# Example
tmp = """\
$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k"""

exampleInputs = process(tmp)
smaller = [size for name,size in exampleInputs.get_directory_sizes() if size <= 100000]
print(sum(smaller))

currentSpace = 70000000 - exampleInputs.size
minToDelete = 30000000 - currentSpace
largeEnough = [size for name,size in exampleInputs.get_directory_sizes() if size >= minToDelete]
print(min(largeEnough))

# Part 1
inputs = process(data)
smaller = [size for name,size in inputs.get_directory_sizes() if size <= 100000]
print(sum(smaller))

# Part 2
currentSpace = 70000000 - inputs.size
minToDelete = 30000000 - currentSpace
largeEnough = [size for name,size in inputs.get_directory_sizes() if size >= minToDelete]
print(min(largeEnough))
