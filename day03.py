from aocd import get_data
data = get_data(day=3)

def process(inputData):
    return [(set(r[:len(r)//2]), set(r[len(r)//2:])) for r in inputData.split('\n')]

inputs = process(data)

priority = ' abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

# Example
tmp = """\
vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw"""

exampleInputs = process(tmp)
leftover = [a.intersection(b).pop() for a,b in exampleInputs]
points = [priority.index(l) for l in leftover]
print(sum(points))

sacks = [a.union(b) for a, b in exampleInputs]
indexer = range(0, len(exampleInputs), 3)
groups = [sacks[i:i+3] for i in indexer]
badges = [set.intersection(*g).pop() for g in groups]
points = [priority.index(b) for b in badges]
print(sum(points))

# Part 1
leftover = [a.intersection(b).pop() for a,b in inputs]
points = [priority.index(l) for l in leftover]
print(sum(points))

# Part 2
sacks = [a.union(b) for a, b in inputs]
indexer = range(0, len(inputs), 3)
groups = [sacks[i:i+3] for i in indexer]
badges = [set.intersection(*g).pop() for g in groups]
points = [priority.index(b) for b in badges]
print(sum(points))
