import numpy as np

from aocd import get_data
data = get_data(day=8)

def process(inputData):
    map = [[int(tree) for tree in row] for row in inputData.split('\n')]
    return np.array(map)

inputs = process(data)

def get_visible_trees(map):
    visible = set()
    nRows, nCols = map.shape
    visible.update({(row, col) for row in (0,nRows-1) for col in range(nCols)})
    visible.update({(row, col) for row in range(nRows) for col in (0,nCols-1)})

    # look from left & right
    for row in range(1,nRows-1):
        # check from left (looking right)
        targetHeight = map[row,0]           # must be greater than edge tree to be seen
        for col in range(1, nCols-1):
            if map[row,col] > targetHeight:
                visible.add((row,col))
                targetHeight = map[row,col]
                if targetHeight >= max(map[row,:]):
                    break
        # check from right (looking left)
        targetHeight = map[row, -1]         # must be greater than edge tree to be seen
        for col in range(nCols-2, 0, -1):
            if map[row, col] > targetHeight:
                visible.add((row, col))
                targetHeight = map[row, col]
                if targetHeight >= max(map[row, :]):
                    break

        # look from top & bottom
        for col in range(1, nCols - 1):
            # check from top (looking down)
            targetHeight = map[0,col]       # must be greater than edge tree to be seen
            for row in range(1, nRows - 1):
                if map[row, col] > targetHeight:
                    visible.add((row, col))
                    targetHeight = map[row, col]
                    if targetHeight >= max(map[:, col]):
                        break
            # check from bottom (looking up)
            targetHeight = map[-1, col]     # must be greater than edge tree to be seen
            for row in range(nRows - 2, 0, -1):
                if map[row, col] > targetHeight:
                    visible.add((row, col))
                    targetHeight = map[row, col]
                    if targetHeight >= max(map[:, col]):
                        break

    return visible

def get_count(mapSlice, height):
    if mapSlice.size == 0:
        return 0
    locs = np.where(mapSlice >= height)[0]
    if locs.size > 0:
        return locs[0]+1
    else:
        return mapSlice.size

def get_scenic_scores(map):
    scores = np.zeros(map.shape, dtype=int)
    nRows, nCols = map.shape
    for row in range(nRows):
        for col in range(nCols):
            height = map[row,col]
            cnt = 1
            # check left
            left = map[row, :col][::-1]
            cnt *= get_count(left, height)
            # check right
            right = map[row, col+1:]
            cnt *= get_count(right, height)
            # check up
            up = map[:row, col][::-1]
            cnt *= get_count(up, height)
            # check down
            down = map[row+1:, col]
            cnt *= get_count(down, height)
            scores[row,col] = cnt
    return scores

# Example
tmp = """\
30373
25512
65332
33549
35390"""

exampleInputs = process(tmp)
visibleTress = get_visible_trees(exampleInputs)
print(len(visibleTress))
scores = get_scenic_scores(exampleInputs)
print(max(scores.flatten()))

# Part 1
visibleTress = get_visible_trees(inputs)
print(len(visibleTress))

# Part 2
scores = get_scenic_scores(inputs)
print(max(scores.flatten()))
