from aocd import get_data
data = get_data(day=13)

def process(inputData):
    packetPairs = []
    for pair in inputData.split('\n\n'):
        left, right = pair.split('\n')
        # python eval will handle nested lists and convert nums to ints
        left = eval(left)
        right = eval(right)
        packetPairs.append((left, right))
    return packetPairs

inputs = process(data)


def compare_packets(left, right):
    leftIsInt = isinstance(left, int)
    rightIsInt = isinstance(right, int)

    # compare ints
    if leftIsInt and rightIsInt:
        if left == right:
            return None
        else:
            return left < right

    # make sure both are lists instead
    if leftIsInt:
        left = [left]
    if rightIsInt:
        right = [right]

    # compare lists
    nLeft = len(left)
    nRight = len(right)
    leftListSmaller = nLeft < nRight if nLeft != nRight else None
    minLen = min(nLeft, nRight)

    # will return here if True/False determined from list
    idx = 0
    while idx < minLen:
        res = compare_packets(left[idx], right[idx])
        if res is not None:
            return res
        idx += 1

    # lastly, will return True/False/None depending on determination
    return leftListSmaller

def get_valid_pair_indexes(packetPairs):
    return [i for i,pair in enumerate(packetPairs, start=1) if compare_packets(*pair)]

def get_packet_list(packetPairs):
    """ for part 2, consider all packets not just pairs, and add the divider packets """
    packets = [pkt for pair in packetPairs for pkt in pair]
    packets.extend([ [[2]], [[6]]] )
    return packets

def sort_packet_list(packetList):
    nPackets = len(packetList)
    reordered = set(range(nPackets))
    idx = -1

    while len(reordered):
        # increment index (and mod to roll around)
        idx += 1
        idx %= (nPackets-1)

        indexes = {idx, idx+1}
        if indexes.intersection(reordered):
            left = packetList[idx]
            right = packetList[idx+1]
            inOrder = compare_packets(left, right)
            if inOrder:
                reordered.difference_update(indexes)    # remove correctly ordered indexes
            else:
                packetList[idx:idx+2] = [right,left]  # flip packets
                reordered.update(indexes)               # add indexes to reordered set

# Example
tmp = """\
[1,1,3,1,1]
[1,1,5,1,1]

[[1],[2,3,4]]
[[1],4]

[9]
[[8,7,6]]

[[4,4],4,4]
[[4,4],4,4,4]

[7,7,7,7]
[7,7,7]

[]
[3]

[[[]]]
[[]]

[1,[2,[3,[4,[5,6,7]]]],8,9]
[1,[2,[3,[4,[5,6,0]]]],8,9]"""

exampleInputs = process(tmp)
validPairs = get_valid_pair_indexes(exampleInputs)
print(sum(validPairs))

packetList = get_packet_list(exampleInputs)
sort_packet_list(packetList)
idx1 = packetList.index([[2]]) + 1
idx2 = packetList.index([[6]]) + 1
print(idx1 * idx2)

# Part 1
validPairs = get_valid_pair_indexes(inputs)
print(sum(validPairs))

# Part 2
packetList = get_packet_list(inputs)
sort_packet_list(packetList)
idx1 = packetList.index([[2]]) + 1
idx2 = packetList.index([[6]]) + 1
print(idx1 * idx2)
