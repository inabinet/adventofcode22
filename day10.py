from aocd import get_data
data = get_data(day=10)

def process(inputData):
    return [command.split(' ') for command in inputData.split('\n')]

inputs = process(data)

def run(commmandList, nCycleToCheck=40, startCycle=20):
    X = 1
    cycle = 0
    sigStrength = []
    image = []
    for command in commmandList:
        cycle += 1
        if (cycle%40) in (X, X+1, X+2):
            pixelLoc = (cycle//40, cycle%40 - 1)
            image.append(pixelLoc)
        if ((cycle-startCycle)%nCycleToCheck) == 0:
            sigStrength.append(X*cycle)
        if command[0] == 'addx':
            cycle += 1
            if (cycle%40) in (X, X+1, X+2):
                pixelLoc = (cycle//40, cycle%40 - 1)
                image.append(pixelLoc)
            if ((cycle-startCycle)%nCycleToCheck) == 0:
                sigStrength.append(X*cycle)
            X += int(command[1])
    return sigStrength, image

def draw_image(image):
    char = '\u2588'
    for row in range(6):
        for col in range(40):
            pix = char if (row,col) in image else ' '
            print(pix,end='')
        print()

# Example
tmp = """\
addx 15
addx -11
addx 6
addx -3
addx 5
addx -1
addx -8
addx 13
addx 4
noop
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx -35
addx 1
addx 24
addx -19
addx 1
addx 16
addx -11
noop
noop
addx 21
addx -15
noop
noop
addx -3
addx 9
addx 1
addx -3
addx 8
addx 1
addx 5
noop
noop
noop
noop
noop
addx -36
noop
addx 1
addx 7
noop
noop
noop
addx 2
addx 6
noop
noop
noop
noop
noop
addx 1
noop
noop
addx 7
addx 1
noop
addx -13
addx 13
addx 7
noop
addx 1
addx -33
noop
noop
noop
addx 2
noop
noop
noop
addx 8
noop
addx -1
addx 2
addx 1
noop
addx 17
addx -9
addx 1
addx 1
addx -3
addx 11
noop
noop
addx 1
noop
addx 1
noop
noop
addx -13
addx -19
addx 1
addx 3
addx 26
addx -30
addx 12
addx -1
addx 3
addx 1
noop
noop
noop
addx -9
addx 18
addx 1
addx 2
noop
noop
addx 9
noop
noop
noop
addx -1
addx 2
addx -37
addx 1
addx 3
noop
addx 15
addx -21
addx 22
addx -6
addx 1
noop
addx 2
addx 1
noop
addx -10
noop
noop
addx 20
addx 1
addx 2
addx 2
addx -6
addx -11
noop
noop
noop"""

exampleInputs = process(tmp)
sigStrength, image = run(exampleInputs)
print(sum(sigStrength))
draw_image(image)

# Part 1
sigStrength, image = run(inputs)
print(sum(sigStrength))

# Part 2
draw_image(image)
