import re

from aocd import get_data
data = get_data(day=5)

def process(inputData):
    return inputData.split('\n\n')

inputs = process(data)


def get_stacks(stackInput):
    stacks = [[]]   # putting empty array at index 0 since stack indexes start at 1
    stackLists = stackInput.split('\n')[::-1]   # reverse so stack index in first row
    indexes = [m.span()[0] for m in re.finditer('\d', stackLists[0])]
    for idx in indexes:
        tmpStack = [row[idx] for row in stackLists[1:] if row[idx] != ' ']
        stacks.append(tmpStack)
    return stacks

def get_steps(stepInput):
    myre = re.compile('move (\d+) from (\d+) to (\d+)')
    stepList = stepInput.split('\n')
    return [[int(i) for i in myre.match(step).groups()] for step in stepList]

def runCrane(stacks, stepList, part1=True):
    for (quantity, src, dst) in stepList:
        toMove = stacks[src][-quantity:]            # get crates to move
        if part1:
            toMove.reverse()                        # revert as this is a LIFO operation (stacks)
        stacks[src] = stacks[src][:-quantity]       # remove crates from source
        stacks[dst] += toMove                       # add crates to destination

def get_crates_on_top(stacks):
    topCrates = [stack[-1] for stack in stacks[1:]]
    topStr = ''.join(topCrates)
    return topStr

# Example
tmp = """\
    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2"""

exampleInputs = process(tmp)
stacks = get_stacks(exampleInputs[0])
stepList = get_steps(exampleInputs[1])
runCrane(stacks, stepList)
print(get_crates_on_top(stacks))
stacks = get_stacks(exampleInputs[0])   # easier to just get them again in original order
runCrane(stacks, stepList, part1=False)
print(get_crates_on_top(stacks))

# Part 1
stacks = get_stacks(inputs[0])
stepList = get_steps(inputs[1])
runCrane(stacks, stepList)
print(get_crates_on_top(stacks))

# Part 2
stacks = get_stacks(inputs[0])
runCrane(stacks, stepList, part1=False)
print(get_crates_on_top(stacks))
