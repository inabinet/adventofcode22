from aocd import get_data
data = get_data(day=1)

def process(inputData):
    return [[int(cal) for cal in elf.split('\n')] for elf in inputData.split('\n\n')]

inputs = process(data)

# Example
tmp = """\
1000
2000
3000

4000

5000
6000

7000
8000
9000

10000"""

exampleInputs = process(tmp)
elfs = [sum(elf) for elf in exampleInputs]
print(max(elfs))
print(sum(sorted(elfs)[-3:]))

# Part 1
elfs = [sum(elf) for elf in inputs]
print(max(elfs))

# Part 2
print(sum(sorted(elfs)[-3:]))
