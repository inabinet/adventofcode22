from aocd import get_data
data = get_data(day=4)

def process(inputData):
    return [[[int(i) for i in p.split('-')] for p in r.split(',')] for r in inputData.split('\n')]

inputs = process(data)

def check_overlap(elf1, elf2):
    if elf1[0] <= elf2[0]:
        if elf1[1] >= elf2[1]:
            return 1
    if elf2[0] <= elf1[0]:
        if elf2[1] >= elf1[1]:
            return 1
    return 0

def check_overlap2(elf1, elf2):
    if elf1[0] <= elf2[0]:
        if elf1[1] < elf2[0]:
            return 0
    if elf2[0] <= elf1[0]:
        if elf2[1] < elf1[0]:
            return 0
    return 1

# Example
tmp = """\
2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8"""

exampleInputs = process(tmp)
overlap = [check_overlap(*pair) for pair in exampleInputs]
print(sum(overlap))
overlap = [check_overlap2(*pair) for pair in exampleInputs]
print(sum(overlap))

# Part 1
overlap = [check_overlap(*pair) for pair in inputs]
print(sum(overlap))

# Part 2
overlap = [check_overlap2(*pair) for pair in inputs]
print(sum(overlap))

